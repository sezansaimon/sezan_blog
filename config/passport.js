const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const { User } = require("../models");
const { generateHash } = require("../helper/passwordHash");
const configure = () => {
  passport.use(
    new LocalStrategy(async function (email, password, callback) {
      const users = await User.findAll({
        where: { email: email },
      });
      const user = users[0];
      if (user.password === generateHash(password)) {
        callback(null, user);
      } else {
        callback(new Error("Invalid credentials"));
      }
    })
  );
};
module.exports = {
  configure,
};

