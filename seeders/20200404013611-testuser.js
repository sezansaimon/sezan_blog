'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [{
      name: 'John',
      email: 'example@example.com',
      phoneNumber: '347',
      password: 'tst',
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  }
};