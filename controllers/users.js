const { User} = require('../models');
const Sequelize = require('sequelize');
const {generatehash} = require('../helper/passwordHash')
const passport = require('passport')
//rendering register form
const renderSignUp = function(req, res) {
    return res.render('users/new.pug');
  }
// 
const registration = async function(req, res) {
  const body = req.body;
  //Await will show its a
  try {
    const user = await User.create({ 
      //call create statically
      name: body.name,
      email: body.email,
      phone: body.phone,
      password: generatehash(body.password)
    });
    res.redirect(302, '/posts');

  } catch (err) {
    console.log('error', err);
    return res.status(504).send({
      error: err.errors[0].message
    })
  }
}
const renderSignIn = function (req, res) {
  return res.render("users/login.pug");
};
const signin = function (req, res) {
  passport.authenticate('local', function(err, user) {
    res.redirect('/posts?message=you being redirected after login');
  })(req, res);

};
module.exports = {
  renderSignUp,
  registration,
  renderSignIn,
  signin,
};