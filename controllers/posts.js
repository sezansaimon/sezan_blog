const {
  Post,
  User
} = require('../models');
const Sequelize = require('sequelize');

const Op = Sequelize.Op;
//MVC
//CRUD
//Read - List, single item
//Create 1 , read 2, upadte 1, delete 1
const renderForm = function(req, res) {
  return res.render('posts/new.pug');
}

const createPost = async function(req, res) {
  const body = req.body;
  console.log(body.title);
  console.log(body.blogDescription);
  //Await will show its a
  try {
    const post = await Post.create({ //call create statically
      title: body.title,
      body: body.blogDescription,
      userID: 1
    });
    res.redirect(302, '/posts');
  } catch (err) {
    console.log('error', err);
    return res.status(504).send({
      error: err.errors[0].message
    })
  }
}
// List of POST - GET,POST
const listPosts = async function(req, res) {
  const posts = await Post.findAll();
  res.render('posts/list.pug', {
    posts,
    qs: req.query.message
  });
}
//Single Post
const singlePost = async function(req, res) {
  const post = await Post.findByPk(req.params.id);
  res.render('posts/singlePost.pug', {
    post
  });
}
//Edit Post Render
const renderEditPost = async function(req, res) {
  const post = await Post.findByPk(req.params.id);
  res.render('posts/editPost.pug', {
    post,
    id: req.params.id
  });
}
//Updating User
const editPost = async function(req, res) {
  const body = req.body;

  // const post = await Post.findByPk(req.params.id);
  // post.title = body.title;
  // post.body = body.blogDescription;
  // await post.save();
  // res.redirect(302, `/posts/${req.params.id}`);

  //original way
  console.log('Test', body, req.params.id);
  //Await will show its a
  try {
    const post = await Post.update({ //call create statically
      title: body.title,
      body: body.blogDescription
    }, {
      where: {
        id: req.params.id
      }
    });
    res.redirect(302, `/posts/${req.params.id}`);
  } catch (err) {
    console.log('error', err);
    return res.status(504).send({
      error: err.errors[0].message
    })
  }
}

//Delete Implementation
const deletePost = async function(req, res) {
  const post = await Post.findByPk(req.params.id);
  await post.destroy();
  res.redirect('/posts?message=delete is done');
}

module.exports = {
  createPost,
  renderForm,
  listPosts,
  singlePost,
  renderEditPost,
  editPost,
  deletePost
}