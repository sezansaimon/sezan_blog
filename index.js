const express = require('express')
const app = express()
const port = 3000
app.set('view engine', 'pug');
const postController = require('./controllers/posts');
const userController = require('./controllers/users');
const bodyParser = require('body-parser');
const passport = require('./config/passport')
passport.configure();
app.use(bodyParser.urlencoded({
  extended: true
}));
//REST
//HTTP Verb - get, post, put, delete, Patch
// respond with "hello world" when a GET request is made to the homepage
app.get('/hello', function(req, res) {
  res.send('hello world')
})

app.get('/infix', function(req, res) {
  res.redirect('http://infixtech.com')
})
//welcome page
app.get('/', (req, res) => res.send('Welcome'))
//new post rendering, creating, listening
app.get('/post', postController.renderForm);
app.post('/post', postController.createPost);
app.get('/posts', postController.listPosts);
//listing single post
app.get('/posts/:id', postController.singlePost)
//editing post
app.get('/post/edit/:id', postController.renderEditPost);
app.put('/post/edit/:id', postController.editPost)
app.post('/post/edit/:id', postController.editPost)
//deleting post
app.get('/post/delete/:id', postController.deletePost);
//signup
app.get('/signup', userController.renderSignUp)
app.post('/signup', userController.registration)
//signin
app.get('/signin', userController.renderSignIn)
app.post('/signin', userController.signin)


app.listen(port, () => console.log(`Blog application listening on port ${port}!`))